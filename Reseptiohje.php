<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fi">
<head>
    <title>Sapuska - Resepti</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="resepti_styles.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        /* Remove the navbar's default margin-bottom and rounded borders */
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
        .row.content {height: 450px}

        /* Set gray background color and 100% height */
        .sidenav {
            padding-top: 20px;
            height: 100%;
        }

        /* Set black background color, white text and some padding */
        footer {
            background-color: #555;
            color: white;
            padding: 15px;
        }

        /* On small screens, set height to 'auto' for sidenav and grid */
        @media screen and (max-width: 767px) {
            .sidenav {
                height: auto;
                padding: 15px;
            }
            .row.content {height:auto;}
        }
    </style>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="Koti.php">KOTI</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <!--Home + alasivut-->
            <ul class="nav navbar-nav">
                <li><a href="Reseptit.php" id="chosen"> Reseptit</a></li> <!-- Kaikki reseptit -->
                <li><a href="Tietoa_meista.php"> Tietoa meistä</a></li>
                <li><a> Haku </a></li>
                <li><a class="search"><form action="hakusivu.php" method="POST"><input type="search" name="submit-search" id="search" placeholder="Hakusana"/></form></a></li>
                <li><a class="sivunnimi center" href="Koti.php">SAPUSKA.FI</a></li>
            </ul>

            <!--Kirjautuminen + rekisteröityminen-->
            <ul class="nav navbar-nav navbar-right">
                <?php
                include 'includes/logregusr.php';
                ?>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-2 sidenav">
           <!-- <p><img src="#">Tähän reseptin kuva</p> -->

            <?php
            include_once 'includes/php-data.php';
            $conn;

            $title = mysqli_real_escape_string($conn, $_GET['title']);

            $sql = "SELECT * FROM recipe WHERE title='$title'";
            $result = mysqli_query($conn,$sql);
            $queryResult = mysqli_num_rows($result);

            while($row = mysqli_fetch_assoc($result)){
                $base64_encoded_image = base64_encode( $row["photo"]);
                echo "<a href='Reseptiohje.php?title=".$row['title']."'></a><div class='col-sm-3'>         
                         <img src=\"data:image/jpg;base64,{$base64_encoded_image}\" width='210' height='150'/>
                          </div>";
                //echo '<img src="data:image/jpg;base64,'.base64_encode($row["photo"]).'">';
            }

            ?>

        </div>
        <div class="col-sm-8 text-left meals" id="reseptikorkeus">

            <?php
            include_once 'includes/php-data.php';
            $conn;

            $title = mysqli_real_escape_string($conn, $_GET['title']);

            $sql = "SELECT * FROM recipe WHERE title='$title'";
            $result = mysqli_query($conn,$sql);
            $queryResult = mysqli_num_rows($result);

            while($row = mysqli_fetch_assoc($result)){
                echo "<a href='Reseptiohje.php?title=".$row['title']."'></a><div class='col-sm-6'>
                          <h1 id='reseptiNimi'>".$row["title"]."</h1>
                          <h2>Valmistusohje</h2>
                          <p>".$row["content"]."</p>
                          
                          </div>";
                //echo '<img src="data:image/jpg;base64,'.base64_encode($row["photo"]).'">';
            }

            ?>
            <?php
            include_once 'includes/php-data.php';
            $conn;

            $title = mysqli_real_escape_string($conn, $_GET['title']);

            $sql = "SELECT * FROM recipe WHERE title='$title'";
            $result = mysqli_query($conn,$sql);
            $queryResult = mysqli_num_rows($result);

            while($row = mysqli_fetch_assoc($result)){
                echo "<a href='Reseptiohje.php?title=".$row['title']."'></a><div class='col-sm-6'>
             <h2>Ainesosat</h2>
             <br>
             <div>".$row["ingredients"]."</div>
             </div>";
            }
            ?>


        </div>
        <div class="col-sm-2 sidenav">
            <div class="well">
                <h2>Suosituimpia reseptejä</h2>
                <p><a href="Reseptiohje.php">Eka resepti</a></p>
                <p><a href="Reseptiohje.php">Toka resepti</a></p>
                <p><a href="Reseptiohje.php">Kolmas resepti</a></p>
            </div>
        </div>
    </div>
</div>

<!-- KIRJAUTUMINEN JA REKISTERÖITYMINEN -->
<?php
include 'includes/form-log-in.php';
?>

<footer class="container-fluid text-center footer">
    <p>© Ryhmä 11</p>
</footer>

</body>
</html>
