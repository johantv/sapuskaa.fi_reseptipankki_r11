<?php
session_start();

include_once 'includes/php-data.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sapuskaa - Koti</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="resepti_styles.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <style>
        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }

        .carousel-inner img {
            width: 100%; /* Set width to 100% */
            min-height: 200px;
        }

        /* Hide the carousel text when the screen is less than 600 pixels wide */
        @media (max-width: 600px) {
            .carousel-caption {
                display: none;
            }
        }


    </style>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="Koti.php" id="chosen">KOTI</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <!--Home + alasivut-->
            <ul class="nav navbar-nav">
                <li><a href="Reseptit.php"> Reseptit</a></li> <!-- Kaikki reseptit -->
                <li><a href="Tietoa_meista.php"> Tietoa meistä</a></li>
                <li><a> Haku </a></li>
                <li><a class="search"><form action="hakusivu.php" method="POST"><input type="search" name="submit-search" id="search" placeholder="Hakusana"/></form></a></li>
                <li><a class="sivunnimi center" href="Koti.php">SAPUSKA.FI</a></li>
            </ul>

            <!--Kirjautuminen + rekisteröityminen + login event lisätty-->
            <ul class="nav navbar-nav navbar-right">
                <?php
                include 'includes/logregusr.php';
                ?>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="pics/burgeri.jpg" alt="Image" width="940" height="528">
                        <div class="carousel-caption">
                            <h3> Texas super vegan meat burger</h3>
                            <p> <a  class="whitelink" href="http://localhost/sapuskadb/reseptiohje.php?title=Kaljaa%20ja%20makkaroita"> Katso resepti</a></p>
                        </div>
                    </div>

                    <div class="item">
                        <img src="pics/vegepasta.jpg" alt="Image" width="940" height="528">
                        <div class="carousel-caption">
                            <h3>Vegepasta</h3>
                            <p> <a  class="whitelink" href="http://localhost/sapuskadb/reseptiohje.php?title=Vegepasta"> Katso resepti</a></p>
                        </div>
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <!--Lista käyttäjistä-->
        <div class="col-sm-4">
            <div class="well wellhigh" >
                <p class="users center">Palveluun rekisteröityneet käyttäjät</p>
                <div class="centerdiv">
                    <table id="kayttajalista">

                        <?php
                        //$conn;
                        $sql = "SELECT name FROM Users LIMIT 20";
                        $result = mysqli_query($conn,$sql);
                        $resultCheck = mysqli_num_rows($result);


                        if ($resultCheck> 0) {
                            // output data of each row
                            while($row = mysqli_fetch_assoc($result)) {

                                echo "<tr></tr><td>".$row["name"]."<br><td>";
                            }
                        } else {
                            echo "0 results";

                        }
                        ?>
                    </table>
                </div>

            </div>

        </div>
    </div>
    <hr>
</div>

<!--Linkit ruokalajeihin-->
<div class="container text-center">
    <h3>Mitä tänään syötäisiin?</h3>
    <br>
    <div class="meals">
        <div class="row">
            <div class="col-sm-3">
                <img src="pics/paaruoka.jpg" class="img-responsive" style="width:100%" alt="Image">
                <h3><a  class="blacklink" href="Reseptit.php"> Pääruoat</a></h3>

            </div>
            <div class="col-sm-3">
                <img src="pics/jalkkarit.jpg" class="img-responsive" style="width:100%" alt="Image">

                <h3><a  class="blacklink" href="Reseptit.php"> Jälkiruoat</a></h3>

            </div>
            <div class="col-sm-3">
                <img src="pics/salado.jpg" class="img-responsive" style="width:100%" alt="Image">

                <h3><a  class="blacklink" href="Reseptit.php"> Salaatit</a></h3>

            </div>
            <div class="col-sm-3">
                <img src="pics/juoma.jpg" class="img-responsive" style="width:100%" alt="Image">

                <h3><a  class="blacklink" href="Reseptit.php"> Juomat</a></h3>

            </div>
        </div>
    </div>
    <hr>
</div>

<!-- KIRJAUTUMINEN JA REKISTERÖITYMINEN -->
<?php
include 'includes/form-log-in.php';
?>


<br>

<footer class="container-fluid text-center footer">
    <p>© Ryhmä 11</p>
</footer>

</body>
</html>
