<?php
session_start();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sapuska - Käyttäjä</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="resepti_styles.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        /* Remove the jumbotron's default bottom margin */
        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>



<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="Koti.php">KOTI</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <!--Home + alasivut-->
            <ul class="nav navbar-nav">
                <li><a href="Reseptit.php" id="chosen"> Reseptit</a></li> <!-- Kaikki reseptit -->
                <li><a href="Tietoa_meista.php"> Tietoa meistä</a></li>
                <li><a> Haku </a></li>
                <li><a class="search"><form action="hakusivu.php" method="POST"><input type="search" name="submit-search" id="search" placeholder="Hakusana"/></form></a></li>
                <li><a class="sivunnimi center" href="Koti.php">SAPUSKA.FI</a></li>
            </ul>

            <!--Kirjautuminen + rekisteröityminen-->
            <ul class="nav navbar-nav navbar-right">
                <?php
                include 'includes/logregusr.php';
                ?>
            </ul>
        </div>
    </div>
</nav>

<div>
    <div class="container text-center kayttajaots">

        <?php
        include_once 'includes/php-data.php';
        $conn;

        $name = $_SESSION['log_name'];//mysqli_real_escape_string($conn, $_GET['name']);

        $sql = "SELECT * FROM users WHERE name='$name'";
        $result = mysqli_query($conn,$sql);
        $queryResult = mysqli_num_rows($result);

        while($row = mysqli_fetch_assoc($result)){
            echo "<h1 id='ktunnuskoko'>".$row["name"]."</h1>";}






        ?>


    </>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-primary kayttajaborders">
                <div class="panel-body "><img src="pics/kokkikuvatesti.jpg" class="img-responsive" style="width:100%" alt="Image"></div>

            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-primary kayttajaborders">
                <div class="omatreseptitkoko center" id="omatreseptit"> Omat reseptit</div>
                <div class="panel-body">


                    <?php
                    include_once 'includes/php-data.php';
                    $conn;

                    $name = $_SESSION['log_name'];//mysqli_real_escape_string($conn, $_GET['name']);

                    $sql = "SELECT * FROM users WHERE name='$name'";
                    $result = mysqli_query($conn,$sql);
                    $queryResult = mysqli_num_rows($result);

                    while($row = mysqli_fetch_assoc($result)){
                    $publisherId = $row["id"];}

                    $sql = "SELECT * FROM recipe WHERE publisherId='$publisherId'";
                    $result = mysqli_query($conn,$sql);
                    $queryResult = mysqli_num_rows($result);

                    while($row = mysqli_fetch_assoc($result)){
                        echo "<a href='reseptiohje.php?title=".$row["title"]."'>
                         <p>".$row["title"]."</p>
                         </a>";}




                    ?>

                       <!-- <p><a href="Reseptiohje.html">Eka resepti</a><i class="material-icons">delete</i></p>
                        <p><a href="Reseptiohje.html">Toka resepti</a><i class="material-icons">delete</i></p>
                        <p><a href="Reseptiohje.html">Kolmas resepti</a><i class="material-icons">delete</i></p>-->
                    </div>
                <div class="panel-footer"></div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-success omatreseptitkoko">
                <!-- tähän reseptin lisääminen -->
                <button  class="omatreseptitkoko" onclick="document.getElementById('resepti').style.display='block'"><a class="lisaaresepti" href="#"><span class="lisaaresepti" ></span>Lisää uusi resepti</a></button>
                <div id="resepti" class="modal">
                    <form class="modal-content animate" action="includes/addrecipe.php" method="POST">

                        <div class="imgcontainer">
                            <h2 class="reseptinlisaysfontti">Lisää uusi resepti</h2>
                            <span onclick="document.getElementById('resepti').style.display='none'" class="close" title="Close Modal">&times;</span>
                        </div>

                        <div class="container">
                            <fieldset>
                                <label class="clear" for="reslaji"><b class="reseptinlisaysfontti reseptinalafontti">Ruokalaji</b></label>
                                <input class="clear" type="text" name="cuisine" placeholder="pääruoka, jälkiruoka, juoma etc..."><br>
                            </fieldset>
                        </div>
                        <div class="container">
                            <label class="clear" for="resnimi"><b class="reseptinlisaysfontti reseptinalafontti">Reseptin nimi</b></label>
                            <input class="clear" type="text" placeholder="Reseptin nimi" name="title" required>

                            <label class="clear" for="resaineet"><b class="reseptinlisaysfontti reseptinalafontti">Ainesosat</b></label>
                            <textarea class="clear" type="text" placeholder="Lisää ainesosat painamalla enteriä" name="ingredients" required></textarea>

                            <label class="clear" for="resohjeet"><b class="reseptinlisaysfontti reseptinalafontti">Valmistusohje</b></label>
                            <textarea class="clear" type="text" placeholder="Lisää reseptin valmistusohje" name="content" required></textarea>

                            <button class="lisaysleveys" type="submit" name="submit">Lisää</button>

                        </div>
                        <div class="container" id="logincontainer">
                            <button  type="button" onclick="document.getElementById('resepti').style.display='none'" class="cancelbtn lisaysleveys">Peruuta</button>

                        </div>
                    </form>
                </div>
                <script>
                    // Get the modal
                    var modal = document.getElementById('resepti');

                    // When the user clicks anywhere outside of the modal, close it
                    window.onclick = function(event) {
                        if (event.target === modal) {
                            modal.style.display = "none";
                        }
                    }
                </script>
            </div>
        </div>
    </div>
</div><br>

<!-- KIRJAUTUMINEN JA REKISTERÖITYMINEN -->
<?php
include 'includes/form-log-in.php';
?>

<br><br>

<footer id="tietoafooter" class="container-fluid text-center footer">
    <p>© Ryhmä 11</p>
</footer>

</body>
</html>
