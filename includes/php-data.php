<?php
/**
 * Created by PhpStorm.
 * User: Yooru
 * Date: 2018/05/06
 * Time: 22:23
 */
$servername = "localhost";
$username = "root";
$password = "";
$dbName = "sapuskadb";

// Create connection
$conn = new mysqli($servername, $username, $password,$dbName);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
