
<!--LOGIN-->
<div id="id01" class="modal">

    <form class="modal-content animate" action="includes/logintry.php" method="POST"">
    <div class="imgcontainer">
        <h2>Kirjaudu</h2>
        <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
        <img src="pics/kokkiuser.PNG" alt="Avatar" class="avatar">
    </div>

    <div class="container">
        <label for="uname"><b>Käyttäjätunnus</b></label>
        <input type="text" placeholder="Käyttäjätunnus" name="logusername" required>

        <label for="psw"><b>Salasana</b></label>
        <input type="password" placeholder="Salasana" name="logpassword" required>

        <button type="submit">Kirjaudu</button>
        <label>
            <input type="checkbox" checked="checked" name="remember"> Remember me
        </label>
    </div>

    <div class="container" id="logincontainer">
        <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Peruuta</button>
        <span class="psw">Unohtuiko <a href="#">salasana?</a></span>
    </div>
    </form>
</div>
<script>
    // Get the modal
    var modal = document.getElementById('id01');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    }
</script>

<!--Login ENDS-->

<!-- REKISTERÖIDY -->

<div id="id02" class="modal">

    <form class="modal-content animate" action="includes/signup.php" method="POST">

        <div class="imgcontainer">
            <h2>Rekisteröidy</h2>
            <span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
            <img src="pics/kokkiuser.PNG" alt="Avatar" class="avatar">
        </div>

        <div class="container">

            <label for="email"><b>Sähköposti</b></label>
            <input type="text" placeholder="Syötä sähköposti" name="email" required>

            <label for="uname"><b>Käyttäjätunnus</b></label>
            <input type="text" placeholder="Syötä käyttäjätunnus" name="username" required>

            <label for="psw"><b>Salasana</b></label>
            <input type="password" placeholder="Syötä salasana" name="password" required>

            <label for="psw"><b></b></label>
            <input type="password" placeholder="Syötä salasana uudelleen..." name="psw">

            <button type="submit" name="submit">Rekisteröidy</button>
            <label>
                <input type="checkbox" name="remember" required> En ole robotti.
            </label>
        </div>
        <div class="container" style="background-color:#f1f1f1">
            <button type="button"  onclick="document.getElementById('id02').style.display='none'" class="cancelbtn">Peruuta</button>
            <span class="psw"><a href="#">Käyttäjäehdot</a></span>
        </div>
    </form>
</div>

<script>
    // Get the modal
    var modal = document.getElementById('id02');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>