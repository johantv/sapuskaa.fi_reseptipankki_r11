<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sapuskaa - Tietoa meistä</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="resepti_styles.css"/>
    <style>
        /* Navbar */
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        /* Footer*/
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="Koti.php">KOTI</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <!--Home + alasivut-->
            <ul class="nav navbar-nav">
                <li ><a href="Reseptit.php"> Reseptit</a></li>
                <li><a href="Tietoa_meista.php" id="chosen"> Tietoa meistä</a></li>
                <li><a> Haku </a></li>
                <li><a class="search"><form action="hakusivu.php" method="POST"><input type="search" name="submit-search" id="search" placeholder="Hakusana"/></form></a></li>
                <li><a class="sivunnimi center" href="Koti.php">SAPUSKA.FI</a></li>
            </ul>

            <!--Kirjautuminen + rekisteröityminen-->
            <ul class="nav navbar-nav navbar-right">
                <?php
                include 'includes/logregusr.php';
                ?>
            </ul>
        </div>
    </div>
</nav>

<div>
    <div class="container text-center">
        <h1 class="reseptihead">Sapuskaa.fi</h1>
        <p class="reseptipar">Tämä sivusto on toteutettu osana Metropolian Web-projekti -kurssia.</p>
    </div>
</div>

<div class="container-fluid bg-3 text-center">
    <br>
    <div class="row">
        <div class="col-sm-3">
            <p>Johanna Virtanen</p>
            <p>Sähköposti: johantv@metropolia.fi</p>
            <p>Puhelin: +358 46 123 4455</p>

        </div>
        <div class="col-sm-3">
            <p>Joonas Lehikoinen</p>
            <p>Sähköposti: joonamle@metropolia.fi</p>
            <p>Puhelin: +358 46 123 4455</p>
        </div>
        <div class="col-sm-3">
            <p>Jaakko Salmi</p>
            <p>Sähköposti: jaakkjs@metropolia.fi</p>
            <p>Puhelin: +358 46 123 4455</p>
        </div>
        <div class="col-sm-3"><p>Samuli Schroderus</p>
            <p>Sähköposti: samulis@metropolia.fi</p>
            <p>Puhelin: +358 46 123 4455</p>
        </div>
    </div>
</div><br>

<!-- KIRJAUTUMINEN JA REKISTERÖITYMINEN -->
<?php
include 'includes/form-log-in.php';
?>

<br><br>

<footer id="tietoafooter" class="container-fluid text-center footer">
    <p>© Ryhmä 11</p>
</footer>


</body>
</html>
