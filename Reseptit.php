<?php
session_start();
include_once 'includes/php-data.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sapuskaa - Haun tulokset</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="resepti_styles.css"/>
    <style>
        /* Remove the navbar's default margin-bottom and rounded borders */
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="Koti.php">KOTI</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <!--Home + alasivut-->
            <ul class="nav navbar-nav">
                <li ><a href="Reseptit.php" id="chosen"> Reseptit</a></li>
                <li><a href="Tietoa_meista.php"> Tietoa meistä</a></li>
                <li><a> Haku </a></li>
                <li><a class="search"><form action="hakusivu.php" method="POST"><input type="search" name="submit-search" id="search" placeholder="Hakusana"/></form></a></li>
                <li><a class="sivunnimi center" href="Koti.php">SAPUSKA.FI</a></li>
            </ul>

            <!--Kirjautuminen + rekisteröityminen-->
            <ul class="nav navbar-nav navbar-right">
                <?php
                include 'includes/logregusr.php';
                ?>
            </ul>
        </div>
    </div>
</nav>

<div>
    <div class="container text-center">
        <h1 class="reseptihead">Reseptihaun tulos</h1>
        <p class="reseptipar">Näytetään kaikki reseptit</p>
    </div>
</div>

<div class="container-fluid bg-3 text-center">
    <br>
    <div class="row">
        <?php

            $sql = "SELECT * FROM recipe LIMIT 20 OFFSET 0";
            $result = mysqli_query($conn,$sql);
            $queryResult = mysqli_num_rows($result);
            if($queryResult>0){

                while($row = mysqli_fetch_assoc($result)){
                    $base64_encoded_image = base64_encode( $row["photo"] );
                    echo "<a href='Reseptiohje.php?title=".$row['title']."'><div class='col-sm-3'>
                          <p>".$row["title"]."</p>
                          
                         <img src=\"data:image/jpg;base64,{$base64_encoded_image}\" width='280' height='180'/>
                          </div></a>";
                    //echo '<img src="data:image/jpg;base64,'.base64_encode($row["photo"]).'">';
                }

            }


        ?>
    </div>
</div><br>

<!-- KIRJAUTUMINEN JA REKISTERÖITYMINEN -->
<?php
include 'includes/form-log-in.php';
?>

<br><br>

<footer id="tietoafooter" class="container-fluid text-center footer">
    <p>© Ryhmä 11</p>
</footer>


</body>
</html>
